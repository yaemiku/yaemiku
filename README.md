Dowcip, żart, kawał – krótka forma humorystyczna, służąca rozśmieszeniu słuchacza. Najczęstszą formą rozpowszechniania dowcipu jest przekaz słowny. Dowcipy można spotkać w różnych stronach internetowych, prasie, w specjalnych rubrykach. Publikowane są książki i czasopisma z dowcipami. Dowcipy można też spotkać na wielu stronach internetowych. Istnieją też ponure żarty („czarny dowcip”) oraz „puste dowcipy”, śmieszące tylko opowiadającego. Innym gatunkiem dowcipu jest „dowcip ciągły”, czyli powtarzany kilka razy w krótkim odstępie czasu lub „dowcip abstrakcyjny”, obecny np. w krajach Europy Środkowej – głównie w Czechach i Polsce. Znany jest także jako pure nonsense czy humor angielski. Można też natrafić na pojęcie żartu „hermetycznego”, który może opierać się na memie lub jest zrozumiały tylko dla docelowej grupy odbiorców, np. kolegów z pracy czy też informatyków.

Zdarzają się okresy popularności dowcipów o konkretnych osobach, np. o Chucku Norrisie w 2005 roku (Chuck Norris Facts). Popularność tematyczna kawałów zmienia się z czasem, w określonym kontekście pojawiają się dowcipy polityczne stanowiące formę ironicznego komentarza wobec rzeczywistości. Zmiany te mają różnorodne przyczyny, a czasem inspiracje. Kawały jako obiekt badań naukowych ujmowane są pomiędzy obszarem trzecim literatury a folklorem.

Prawdopodobnie najstarszą księgą żartów jest anonimowy zbiór dowcipów pt. „Philogelos” (gr. Φιλόγελως, dosł. miłujący śmiech) pochodząca z III/IV w. Znajduje się w nim 265 kawałów.

Według André Jollesa żart jest jednym z morfologicznych archetypów leżących u podstaw współczesnych gatunków literackich.

Jedną z największych monografii o dowcipie napisał Zygmunt Freud, w której opisał dowcip w relacji do nieświadomości. 
